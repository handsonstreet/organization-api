'use strict';

const chai = require('chai'),
      sinon = require('sinon'),
      expect = chai.expect
chai.should();



describe('Report', function(){
    let ReportSchema = require('../../lib/models/report');
    const esClient = require("../../lib/elasticsearch");

    let reportList = [
        {
            "_id": "5cf469cc9db1c9619c6c8933",
            "name": "Detailed connections",
            "description": "Detailed connection events",
            "type": "detailed",
            "body": {
                "size": 10000,
                "query": {
                    "bool": {
                        "must": [
                            {
                                "range": {
                                    "timestamp": {
                                        "gte": "now-1d",
                                        "lte": "now",
                                        "format": "epoch_millis"
                                    }
                                }
                            }
                        ]
                    }
                }
            },
            "__v": 0,
            "filtdoers": [
                "today"
            ]
        },
        {
            "_id": "5cf469cc9db1c9619c6c8934",
            "name": "Connections by hour",
            "description": "Count of connections grouped by hour",
            "type": "connections_by_hour",
            "body": {
                "size": 0,
                "query": {
                    "bool": {
                        "must": [
                            {
                                "match": {
                                    "action": {
                                        "query": "connect"
                                    }
                                }
                            },
                            {
                                "range": {
                                    "timestamp": {
                                        "gte": "now-1d",
                                        "lte": "now",
                                        "format": "epoch_millis"
                                    }
                                }
                            }
                        ]
                    }
                },
                "aggs": {
                    "group_by_hour": {
                        "date_histogram": {
                            "field": "timestamp",
                            "interval": "hour"
                        }
                    }
                }
            },
            "__v": 0,
            "filters": [
                "today"
            ]
        },
        {
            "_id": "5cf469cc9db1c9619c6c8935",
            "name": "Connections by ip",
            "description": "Detailed of connections grouped by ip",
            "type": "connections_by_ip",
            "body": {
                "size": 0,
                "query": {
                    "bool": {
                        "must": [
                            {
                                "range": {
                                    "timestamp": {
                                        "gte": "now-1d",
                                        "lte": "now",
                                        "format": "epoch_millis"
                                    }
                                }
                            }
                        ]
                    }
                },
                "aggs": {
                    "group_by_ip": {
                        "terms": {
                            "field": "ip"
                        },
                        "aggs": {
                            "group_by_secuence": {
                                "terms": {
                                    "field": "secuence"
                                },
                                "aggs": {
                                    "disconnection_time": {
                                        "max": {
                                            "field": "timestamp"
                                        }
                                    },
                                    "connection_time": {
                                        "min": {
                                            "field": "timestamp"
                                        }
                                    },
                                    "duration_in_seconds": {
                                        "bucket_script": {
                                            "buckets_path": {
                                                "start_time": "connection_time",
                                                "end_time": "disconnection_time"
                                            },
                                            "script": "(params.end_time - params.start_time)/1000"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "__v": 0,
            "filters": [
                "today"
            ]
        }
    ]
        
    let mockFind = {
        exec: function () {
        return Promise.resolve(reportList);
    }
    };
    let mockFindOne = 
    {
        exec: function () {
            let reportValue = new Promise((resolve, reject) => {
                reportList.forEach(report => {
                        if(report.type === 'connections_by_ip'){
                            resolve(report);
                        }
                });
            });
            return reportValue; 
        }
    }
    
    function mockFindById (selectedId, callback) {
            let foundedReport;
            reportList.forEach(report => {
                    if(report._id === selectedId){
                        foundedReport = report;
                    }
            });
            return callback(null, foundedReport);
    }
    
    let reportEsRetults = [
        {
        "took": 41,
        "timed_out": false,
        "_shards": {
            "total": 2,
            "successful": 2,
            "skipped": 0,
            "failed": 0
        },
        "hits": {
            "total": 53,
            "max_score": 0,
            "hits": []
        },
        "aggregations": {
            "group_by_ip": {
                "doc_count_error_upper_bound": 0,
                "sum_other_doc_count": 0,
                "buckets": [
                    {
                        "key": "190.210.102.151",
                        "doc_count": 18,
                        "group_by_secuence": {
                            "doc_count_error_upper_bound": 0,
                            "sum_other_doc_count": 0,
                            "buckets": [
                                {
                                    "key": "5OR0nthg2ZaO_vaeAAAS",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559829718926,
                                        "value_as_string": "1559829718926"
                                    },
                                    "disconnection_time": {
                                        "value": 1559829771908,
                                        "value_as_string": "1559829771908"
                                    },
                                    "duration_in_seconds": {
                                        "value": 52.982
                                    }
                                },
                                {
                                    "key": "7mkvFUsOcNPoTQmFAAAJ",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559779957619,
                                        "value_as_string": "1559779957619"
                                    },
                                    "disconnection_time": {
                                        "value": 1559780011485,
                                        "value_as_string": "1559780011485"
                                    },
                                    "duration_in_seconds": {
                                        "value": 53.866
                                    }
                                },
                                {
                                    "key": "A8n2Us-YWiFm9DQ8AAAZ",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559843585430,
                                        "value_as_string": "1559843585430"
                                    },
                                    "disconnection_time": {
                                        "value": 1559844012144,
                                        "value_as_string": "1559844012144"
                                    },
                                    "duration_in_seconds": {
                                        "value": 426.714
                                    }
                                },
                                {
                                    "key": "GXE1mEF322rh3O_lAAAL",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559820090605,
                                        "value_as_string": "1559820090605"
                                    },
                                    "disconnection_time": {
                                        "value": 1559820343908,
                                        "value_as_string": "1559820343908"
                                    },
                                    "duration_in_seconds": {
                                        "value": 253.303
                                    }
                                },
                                {
                                    "key": "Lo_kgk_xlmZyNZqPAAAb",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559844463291,
                                        "value_as_string": "1559844463291"
                                    },
                                    "disconnection_time": {
                                        "value": 1559844464528,
                                        "value_as_string": "1559844464528"
                                    },
                                    "duration_in_seconds": {
                                        "value": 1.237
                                    }
                                },
                                {
                                    "key": "Q4pujOnC56B7O5LEAAAM",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559820249545,
                                        "value_as_string": "1559820249545"
                                    },
                                    "disconnection_time": {
                                        "value": 1559820377869,
                                        "value_as_string": "1559820377869"
                                    },
                                    "duration_in_seconds": {
                                        "value": 128.324
                                    }
                                },
                                {
                                    "key": "UdvtJZCcdW2dXiF8AAAK",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559820061277,
                                        "value_as_string": "1559820061277"
                                    },
                                    "disconnection_time": {
                                        "value": 1559820090165,
                                        "value_as_string": "1559820090165"
                                    },
                                    "duration_in_seconds": {
                                        "value": 28.888
                                    }
                                },
                                {
                                    "key": "vJ7WcuP5pnJjKP3OAAAI",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559776291787,
                                        "value_as_string": "1559776291787"
                                    },
                                    "disconnection_time": {
                                        "value": 1559777844048,
                                        "value_as_string": "1559777844048"
                                    },
                                    "duration_in_seconds": {
                                        "value": 1552.261
                                    }
                                },
                                {
                                    "key": "zi_6lySysVlj6pGwAAAa",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559844013192,
                                        "value_as_string": "1559844013192"
                                    },
                                    "disconnection_time": {
                                        "value": 1559844015634,
                                        "value_as_string": "1559844015634"
                                    },
                                    "duration_in_seconds": {
                                        "value": 2.442
                                    }
                                }
                            ]
                        }
                    },
                    {
                        "key": "190.210.239.58",
                        "doc_count": 10,
                        "group_by_secuence": {
                            "doc_count_error_upper_bound": 0,
                            "sum_other_doc_count": 0,
                            "buckets": [
                                {
                                    "key": "2Yq18duRTRZlpzytAAAH",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559771429737,
                                        "value_as_string": "1559771429737"
                                    },
                                    "disconnection_time": {
                                        "value": 1559771707829,
                                        "value_as_string": "1559771707829"
                                    },
                                    "duration_in_seconds": {
                                        "value": 278.092
                                    }
                                },
                                {
                                    "key": "38x0Gy52zinX_XrOAAAA",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559762423014,
                                        "value_as_string": "1559762423014"
                                    },
                                    "disconnection_time": {
                                        "value": 1559762434927,
                                        "value_as_string": "1559762434927"
                                    },
                                    "duration_in_seconds": {
                                        "value": 11.913
                                    }
                                },
                                {
                                    "key": "IhOL3NsQH5RZ2nUuAAAC",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559762461177,
                                        "value_as_string": "1559762461177"
                                    },
                                    "disconnection_time": {
                                        "value": 1559762738962,
                                        "value_as_string": "1559762738962"
                                    },
                                    "duration_in_seconds": {
                                        "value": 277.785
                                    }
                                },
                                {
                                    "key": "Y4ZCG2c9ZDGYdCyaAAAD",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559762472551,
                                        "value_as_string": "1559762472551"
                                    },
                                    "disconnection_time": {
                                        "value": 1559762826938,
                                        "value_as_string": "1559762826938"
                                    },
                                    "duration_in_seconds": {
                                        "value": 354.387
                                    }
                                },
                                {
                                    "key": "t_3LM-d8dNXpvAfPAAAB",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559762435334,
                                        "value_as_string": "1559762435334"
                                    },
                                    "disconnection_time": {
                                        "value": 1559762740628,
                                        "value_as_string": "1559762740628"
                                    },
                                    "duration_in_seconds": {
                                        "value": 305.294
                                    }
                                }
                            ]
                        }
                    },
                    {
                        "key": "80.28.110.143",
                        "doc_count": 10,
                        "group_by_secuence": {
                            "doc_count_error_upper_bound": 0,
                            "sum_other_doc_count": 0,
                            "buckets": [
                                {
                                    "key": "PGnJ-pE_5SIXwtscAAAW",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559832966603,
                                        "value_as_string": "1559832966603"
                                    },
                                    "disconnection_time": {
                                        "value": 1559832968250,
                                        "value_as_string": "1559832968250"
                                    },
                                    "duration_in_seconds": {
                                        "value": 1.647
                                    }
                                },
                                {
                                    "key": "W3H7qx7vuqARmlp4AAAN",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559826564987,
                                        "value_as_string": "1559826564987"
                                    },
                                    "disconnection_time": {
                                        "value": 1559826662533,
                                        "value_as_string": "1559826662533"
                                    },
                                    "duration_in_seconds": {
                                        "value": 97.546
                                    }
                                },
                                {
                                    "key": "WAXMEOj3vE68TUo5AAAX",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559832980961,
                                        "value_as_string": "1559832980961"
                                    },
                                    "disconnection_time": {
                                        "value": 1559832983345,
                                        "value_as_string": "1559832983345"
                                    },
                                    "duration_in_seconds": {
                                        "value": 2.384
                                    }
                                },
                                {
                                    "key": "WDWU5Q-FV8zA1vwtAAAP",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559826696140,
                                        "value_as_string": "1559826696140"
                                    },
                                    "disconnection_time": {
                                        "value": 1559826697683,
                                        "value_as_string": "1559826697683"
                                    },
                                    "duration_in_seconds": {
                                        "value": 1.543
                                    }
                                },
                                {
                                    "key": "b17lrBwRisVDG_jGAAAV",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559832623223,
                                        "value_as_string": "1559832623223"
                                    },
                                    "disconnection_time": {
                                        "value": 1559832820384,
                                        "value_as_string": "1559832820384"
                                    },
                                    "duration_in_seconds": {
                                        "value": 197.161
                                    }
                                }
                            ]
                        }
                    },
                    {
                        "key": "216.199.92.50",
                        "doc_count": 8,
                        "group_by_secuence": {
                            "doc_count_error_upper_bound": 0,
                            "sum_other_doc_count": 0,
                            "buckets": [
                                {
                                    "key": "d-0f9EAUHZe5MJkSAAAT",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559829809964,
                                        "value_as_string": "1559829809964"
                                    },
                                    "disconnection_time": {
                                        "value": 1559832345024,
                                        "value_as_string": "1559832345024"
                                    },
                                    "duration_in_seconds": {
                                        "value": 2535.06
                                    }
                                },
                                {
                                    "key": "jjgO3__dinahy3n6AAAG",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559763272754,
                                        "value_as_string": "1559763272754"
                                    },
                                    "disconnection_time": {
                                        "value": 1559763352583,
                                        "value_as_string": "1559763352583"
                                    },
                                    "duration_in_seconds": {
                                        "value": 79.829
                                    }
                                },
                                {
                                    "key": "mMOXzVJwlja6UJM6AAAF",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559763206943,
                                        "value_as_string": "1559763206943"
                                    },
                                    "disconnection_time": {
                                        "value": 1559763260033,
                                        "value_as_string": "1559763260033"
                                    },
                                    "duration_in_seconds": {
                                        "value": 53.09
                                    }
                                },
                                {
                                    "key": "uec1naRjeqtik5WkAAAR",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559828356432,
                                        "value_as_string": "1559828356432"
                                    },
                                    "disconnection_time": {
                                        "value": 1559829808424,
                                        "value_as_string": "1559829808424"
                                    },
                                    "duration_in_seconds": {
                                        "value": 1451.992
                                    }
                                }
                            ]
                        }
                    },
                    {
                        "key": "181.9.123.59",
                        "doc_count": 2,
                        "group_by_secuence": {
                            "doc_count_error_upper_bound": 0,
                            "sum_other_doc_count": 0,
                            "buckets": [
                                {
                                    "key": "MfnZ7seQFnZ2bwr8AAAE",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559762764772,
                                        "value_as_string": "1559762764772"
                                    },
                                    "disconnection_time": {
                                        "value": 1559762798131,
                                        "value_as_string": "1559762798131"
                                    },
                                    "duration_in_seconds": {
                                        "value": 33.359
                                    }
                                }
                            ]
                        }
                    },
                    {
                        "key": "80.251.161.186",
                        "doc_count": 2,
                        "group_by_secuence": {
                            "doc_count_error_upper_bound": 0,
                            "sum_other_doc_count": 0,
                            "buckets": [
                                {
                                    "key": "wLLWXDK_2Fi8gocxAAAY",
                                    "doc_count": 2,
                                    "connection_time": {
                                        "value": 1559841087993,
                                        "value_as_string": "1559841087993"
                                    },
                                    "disconnection_time": {
                                        "value": 1559841345733,
                                        "value_as_string": "1559841345733"
                                    },
                                    "duration_in_seconds": {
                                        "value": 257.74
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }
        },
        {
            "took": 9,
            "timed_out": false,
            "_shards": {
                "total": 2,
                "successful": 2,
                "skipped": 0,
                "failed": 0
            },
            "hits": {
                "total": 18,
                "max_score": 0,
                "hits": []
            },
            "aggregations": {
                "group_by_hour": {
                    "buckets": [
                        {
                            "key_as_string": "1559768400000",
                            "key": 1559768400000,
                            "doc_count": 1
                        },
                        {
                            "key_as_string": "1559772000000",
                            "key": 1559772000000,
                            "doc_count": 0
                        },
                        {
                            "key_as_string": "1559775600000",
                            "key": 1559775600000,
                            "doc_count": 1
                        },
                        {
                            "key_as_string": "1559779200000",
                            "key": 1559779200000,
                            "doc_count": 1
                        },
                        {
                            "key_as_string": "1559782800000",
                            "key": 1559782800000,
                            "doc_count": 0
                        },
                        {
                            "key_as_string": "1559786400000",
                            "key": 1559786400000,
                            "doc_count": 0
                        },
                        {
                            "key_as_string": "1559790000000",
                            "key": 1559790000000,
                            "doc_count": 0
                        },
                        {
                            "key_as_string": "1559793600000",
                            "key": 1559793600000,
                            "doc_count": 0
                        },
                        {
                            "key_as_string": "1559797200000",
                            "key": 1559797200000,
                            "doc_count": 0
                        },
                        {
                            "key_as_string": "1559800800000",
                            "key": 1559800800000,
                            "doc_count": 0
                        },
                        {
                            "key_as_string": "1559804400000",
                            "key": 1559804400000,
                            "doc_count": 0
                        },
                        {
                            "key_as_string": "1559808000000",
                            "key": 1559808000000,
                            "doc_count": 0
                        },
                        {
                            "key_as_string": "1559811600000",
                            "key": 1559811600000,
                            "doc_count": 0
                        },
                        {
                            "key_as_string": "1559815200000",
                            "key": 1559815200000,
                            "doc_count": 0
                        },
                        {
                            "key_as_string": "1559818800000",
                            "key": 1559818800000,
                            "doc_count": 3
                        },
                        {
                            "key_as_string": "1559822400000",
                            "key": 1559822400000,
                            "doc_count": 0
                        },
                        {
                            "key_as_string": "1559826000000",
                            "key": 1559826000000,
                            "doc_count": 3
                        },
                        {
                            "key_as_string": "1559829600000",
                            "key": 1559829600000,
                            "doc_count": 5
                        },
                        {
                            "key_as_string": "1559833200000",
                            "key": 1559833200000,
                            "doc_count": 0
                        },
                        {
                            "key_as_string": "1559836800000",
                            "key": 1559836800000,
                            "doc_count": 0
                        },
                        {
                            "key_as_string": "1559840400000",
                            "key": 1559840400000,
                            "doc_count": 2
                        },
                        {
                            "key_as_string": "1559844000000",
                            "key": 1559844000000,
                            "doc_count": 2
                        }
                    ]
                }
            }
        },
        {
            "took": 34,
            "timed_out": false,
            "_shards": {
                "total": 2,
                "successful": 2,
                "skipped": 0,
                "failed": 0
            },
            "hits": {
                "total": 39,
                "max_score": 1,
                "hits": [
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "7xWjKWsB0Jekxiwtefi6",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.239.58",
                            "action": "disconnect",
                            "timestamp": 1559771707829,
                            "secuence": "2Yq18duRTRZlpzytAAAH"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "7hWfKWsB0JekxiwtO_hr",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.239.58",
                            "action": "connect",
                            "timestamp": 1559771429737,
                            "secuence": "2Yq18duRTRZlpzytAAAH"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "8hUiKmsB0JekxiwtLfjg",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.102.151",
                            "action": "disconnect",
                            "timestamp": 1559780011485,
                            "secuence": "7mkvFUsOcNPoTQmFAAAJ"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "vnyFLGsB5L26GDTYu983",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.102.151",
                            "action": "disconnect",
                            "timestamp": 1559820090165,
                            "secuence": "UdvtJZCcdW2dXiF8AAAK"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "9BWKLGsB0JekxiwtH_gP",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.102.151",
                            "action": "disconnect",
                            "timestamp": 1559820377869,
                            "secuence": "Q4pujOnC56B7O5LEAAAM"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "-RXqLGsB0JekxiwtjfjV",
                        "_score": 1,
                        "_source": {
                            "ip": "80.28.110.143",
                            "action": "disconnect",
                            "timestamp": 1559826697683,
                            "secuence": "WDWU5Q-FV8zA1vwtAAAP"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "xHwaLWsB5L26GDTYBd8r",
                        "_score": 1,
                        "_source": {
                            "ip": "216.199.92.50",
                            "action": "disconnect",
                            "timestamp": 1559829808424,
                            "secuence": "uec1naRjeqtik5WkAAAR"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "wnwYLWsB5L26GDTYp9-P",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.102.151",
                            "action": "connect",
                            "timestamp": 1559829718926,
                            "secuence": "5OR0nthg2ZaO_vaeAAAS"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "_RVELWsB0Jekxiwt-Ph5",
                        "_score": 1,
                        "_source": {
                            "ip": "80.28.110.143",
                            "action": "connect",
                            "timestamp": 1559832623223,
                            "secuence": "b17lrBwRisVDG_jGAAAV"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "-hUDLWsB0Jekxiwt3fhS",
                        "_score": 1,
                        "_source": {
                            "ip": "216.199.92.50",
                            "action": "connect",
                            "timestamp": 1559828356432,
                            "secuence": "uec1naRjeqtik5WkAAAR"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "wXzrLGsB5L26GDTYtN8p",
                        "_score": 1,
                        "_source": {
                            "action": "disconnect",
                            "timestamp": 1559826773030,
                            "secuence": "DaaoPJkdopfHPDZjAAAQ"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "ABVKLWsB0Jekxiwtbfni",
                        "_score": 1,
                        "_source": {
                            "ip": "80.28.110.143",
                            "action": "connect",
                            "timestamp": 1559832980961,
                            "secuence": "WAXMEOj3vE68TUo5AAAX"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "ARVKLWsB0Jekxiwtd_ky",
                        "_score": 1,
                        "_source": {
                            "ip": "80.28.110.143",
                            "action": "disconnect",
                            "timestamp": 1559832983345,
                            "secuence": "WAXMEOj3vE68TUo5AAAX"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "AxXKLWsB0JekxiwtEPnK",
                        "_score": 1,
                        "_source": {
                            "ip": "80.251.161.186",
                            "action": "disconnect",
                            "timestamp": 1559841345733,
                            "secuence": "wLLWXDK_2Fi8gocxAAAY"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "x3zyLWsB5L26GDTYwN92",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.102.151",
                            "action": "disconnect",
                            "timestamp": 1559844012144,
                            "secuence": "A8n2Us-YWiFm9DQ8AAAZ"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "yHzyLWsB5L26GDTYxN-J",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.102.151",
                            "action": "connect",
                            "timestamp": 1559844013192,
                            "secuence": "zi_6lySysVlj6pGwAAAa"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "yXzyLWsB5L26GDTYzt8U",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.102.151",
                            "action": "disconnect",
                            "timestamp": 1559844015634,
                            "secuence": "zi_6lySysVlj6pGwAAAa"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "BRX5LWsB0Jekxiwtovm8",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.102.151",
                            "action": "connect",
                            "timestamp": 1559844463291,
                            "secuence": "Lo_kgk_xlmZyNZqPAAAb"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "BhX5LWsB0Jekxiwtp_mR",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.102.151",
                            "action": "disconnect",
                            "timestamp": 1559844464528,
                            "secuence": "Lo_kgk_xlmZyNZqPAAAb"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "vHwBKmsB5L26GDTYG99W",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.102.151",
                            "action": "disconnect",
                            "timestamp": 1559777844048,
                            "secuence": "vJ7WcuP5pnJjKP3OAAAI"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "8BXpKWsB0Jekxiwta_jN",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.102.151",
                            "action": "connect",
                            "timestamp": 1559776291787,
                            "secuence": "vJ7WcuP5pnJjKP3OAAAI"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "8RUhKmsB0JekxiwtW_h1",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.102.151",
                            "action": "connect",
                            "timestamp": 1559779957619,
                            "secuence": "7mkvFUsOcNPoTQmFAAAJ"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "vXyFLGsB5L26GDTYSt9h",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.102.151",
                            "action": "connect",
                            "timestamp": 1559820061277,
                            "secuence": "UdvtJZCcdW2dXiF8AAAK"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "8xWJLGsB0Jekxiwtmvhn",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.102.151",
                            "action": "disconnect",
                            "timestamp": 1559820343908,
                            "secuence": "GXE1mEF322rh3O_lAAAL"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "w3wZLWsB5L26GDTYdt-G",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.102.151",
                            "action": "disconnect",
                            "timestamp": 1559829771908,
                            "secuence": "5OR0nthg2ZaO_vaeAAAS"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "9xXqLGsB0JekxiwtBPiH",
                        "_score": 1,
                        "_source": {
                            "ip": "80.28.110.143",
                            "action": "disconnect",
                            "timestamp": 1559826662533,
                            "secuence": "W3H7qx7vuqARmlp4AAAN"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "v3yFLGsB5L26GDTYvN_w",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.102.151",
                            "action": "connect",
                            "timestamp": 1559820090605,
                            "secuence": "GXE1mEF322rh3O_lAAAL"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "wHyILGsB5L26GDTYKd_K",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.102.151",
                            "action": "connect",
                            "timestamp": 1559820249545,
                            "secuence": "Q4pujOnC56B7O5LEAAAM"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "9RXoLGsB0Jekxiwth_h9",
                        "_score": 1,
                        "_source": {
                            "ip": "80.28.110.143",
                            "action": "connect",
                            "timestamp": 1559826564987,
                            "secuence": "W3H7qx7vuqARmlp4AAAN"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "-BXqLGsB0Jekxiwth_jN",
                        "_score": 1,
                        "_source": {
                            "ip": "80.28.110.143",
                            "action": "connect",
                            "timestamp": 1559826696140,
                            "secuence": "WDWU5Q-FV8zA1vwtAAAP"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "xXwaLWsB5L26GDTYC98u",
                        "_score": 1,
                        "_source": {
                            "ip": "216.199.92.50",
                            "action": "connect",
                            "timestamp": 1559829809964,
                            "secuence": "d-0f9EAUHZe5MJkSAAAT"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "9hXpLGsB0JekxiwtevhT",
                        "_score": 1,
                        "_source": {
                            "action": "disconnect",
                            "timestamp": 1559826627153,
                            "secuence": "5-mth55vACbWj68MAAAO"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "-xVALWsB0JekxiwtufjJ",
                        "_score": 1,
                        "_source": {
                            "ip": "216.199.92.50",
                            "action": "disconnect",
                            "timestamp": 1559832345024,
                            "secuence": "d-0f9EAUHZe5MJkSAAAT"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "_BVELWsB0JekxiwtHfgx",
                        "_score": 1,
                        "_source": {
                            "action": "disconnect",
                            "timestamp": 1559832567086,
                            "secuence": "r8fjw40hsxz1umm0AAAU"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "xnxHLWsB5L26GDTY-t-n",
                        "_score": 1,
                        "_source": {
                            "ip": "80.28.110.143",
                            "action": "disconnect",
                            "timestamp": 1559832820384,
                            "secuence": "b17lrBwRisVDG_jGAAAV"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "_hVKLWsB0JekxiwtNfjM",
                        "_score": 1,
                        "_source": {
                            "ip": "80.28.110.143",
                            "action": "connect",
                            "timestamp": 1559832966603,
                            "secuence": "PGnJ-pE_5SIXwtscAAAW"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "_xVKLWsB0JekxiwtPPg7",
                        "_score": 1,
                        "_source": {
                            "ip": "80.28.110.143",
                            "action": "disconnect",
                            "timestamp": 1559832968250,
                            "secuence": "PGnJ-pE_5SIXwtscAAAW"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "AhXGLWsB0JekxiwtIfn7",
                        "_score": 1,
                        "_source": {
                            "ip": "80.251.161.186",
                            "action": "connect",
                            "timestamp": 1559841087993,
                            "secuence": "wLLWXDK_2Fi8gocxAAAY"
                        }
                    },
                    {
                        "_index": "connection",
                        "_type": "_doc",
                        "_id": "BBXsLWsB0JekxiwtPfmY",
                        "_score": 1,
                        "_source": {
                            "ip": "190.210.102.151",
                            "action": "connect",
                            "timestamp": 1559843585430,
                            "secuence": "A8n2Us-YWiFm9DQ8AAAZ"
                        }
                    }
                ]
            }
        }
    ];
    function mockEsSearch(query) {
        let reportValue = new Promise((resolve, reject) => {             
            if(!query.body.aggs){
                resolve(reportEsRetults[2]);
            } else if(query.body.aggs.group_by_ip){
                resolve(reportEsRetults[0]);
            } else if(query.body.aggs.group_by_hour){
                resolve(reportEsRetults[1]);
            } 
        });
        return reportValue; 
    }
    
    let procesedReports = {
        "connections_by_ip": [
            {
                "ip": "190.210.102.151",
                "events": [
                    {
                        "start": 1559829718926,
                        "end": 1559829771908,
                        "duration": 52.982
                    },
                    {
                        "start": 1559779957619,
                        "end": 1559780011485,
                        "duration": 53.866
                    },
                    {
                        "start": 1559843585430,
                        "end": 1559844012144,
                        "duration": 426.714
                    },
                    {
                        "start": 1559820090605,
                        "end": 1559820343908,
                        "duration": 253.303
                    },
                    {
                        "start": 1559844463291,
                        "end": 1559844464528,
                        "duration": 1.237
                    },
                    {
                        "start": 1559820249545,
                        "end": 1559820377869,
                        "duration": 128.324
                    },
                    {
                        "start": 1559820061277,
                        "end": 1559820090165,
                        "duration": 28.888
                    },
                    {
                        "start": 1559776291787,
                        "end": 1559777844048,
                        "duration": 1552.261
                    },
                    {
                        "start": 1559844013192,
                        "end": 1559844015634,
                        "duration": 2.442
                    }
                ]
            },
            {
                "ip": "190.210.239.58",
                "events": [
                    {
                        "start": 1559771429737,
                        "end": 1559771707829,
                        "duration": 278.092
                    },
                    {
                        "start": 1559762423014,
                        "end": 1559762434927,
                        "duration": 11.913
                    },
                    {
                        "start": 1559762461177,
                        "end": 1559762738962,
                        "duration": 277.785
                    },
                    {
                        "start": 1559762472551,
                        "end": 1559762826938,
                        "duration": 354.387
                    },
                    {
                        "start": 1559762435334,
                        "end": 1559762740628,
                        "duration": 305.294
                    }
                ]
            },
            {
                "ip": "80.28.110.143",
                "events": [
                    {
                        "start": 1559832966603,
                        "end": 1559832968250,
                        "duration": 1.647
                    },
                    {
                        "start": 1559826564987,
                        "end": 1559826662533,
                        "duration": 97.546
                    },
                    {
                        "start": 1559832980961,
                        "end": 1559832983345,
                        "duration": 2.384
                    },
                    {
                        "start": 1559826696140,
                        "end": 1559826697683,
                        "duration": 1.543
                    },
                    {
                        "start": 1559832623223,
                        "end": 1559832820384,
                        "duration": 197.161
                    }
                ]
            },
            {
                "ip": "216.199.92.50",
                "events": [
                    {
                        "start": 1559829809964,
                        "end": 1559832345024,
                        "duration": 2535.06
                    },
                    {
                        "start": 1559763272754,
                        "end": 1559763352583,
                        "duration": 79.829
                    },
                    {
                        "start": 1559763206943,
                        "end": 1559763260033,
                        "duration": 53.09
                    },
                    {
                        "start": 1559828356432,
                        "end": 1559829808424,
                        "duration": 1451.992
                    }
                ]
            },
            {
                "ip": "181.9.123.59",
                "events": [
                    {
                        "start": 1559762764772,
                        "end": 1559762798131,
                        "duration": 33.359
                    }
                ]
            },
            {
                "ip": "80.251.161.186",
                "events": [
                    {
                        "start": 1559841087993,
                        "end": 1559841345733,
                        "duration": 257.74
                    }
                ]
            }
        ]
        ,"connections_by_hour": [
            {
                "hour": 1559768400000,
                "count": 1
            },
            {
                "hour": 1559772000000,
                "count": 0
            },
            {
                "hour": 1559775600000,
                "count": 1
            },
            {
                "hour": 1559779200000,
                "count": 1
            },
            {
                "hour": 1559782800000,
                "count": 0
            },
            {
                "hour": 1559786400000,
                "count": 0
            },
            {
                "hour": 1559790000000,
                "count": 0
            },
            {
                "hour": 1559793600000,
                "count": 0
            },
            {
                "hour": 1559797200000,
                "count": 0
            },
            {
                "hour": 1559800800000,
                "count": 0
            },
            {
                "hour": 1559804400000,
                "count": 0
            },
            {
                "hour": 1559808000000,
                "count": 0
            },
            {
                "hour": 1559811600000,
                "count": 0
            },
            {
                "hour": 1559815200000,
                "count": 0
            },
            {
                "hour": 1559818800000,
                "count": 3
            },
            {
                "hour": 1559822400000,
                "count": 0
            },
            {
                "hour": 1559826000000,
                "count": 3
            },
            {
                "hour": 1559829600000,
                "count": 5
            },
            {
                "hour": 1559833200000,
                "count": 0
            },
            {
                "hour": 1559836800000,
                "count": 0
            },
            {
                "hour": 1559840400000,
                "count": 2
            },
            {
                "hour": 1559844000000,
                "count": 2
            }
        ]
        ,"detailed": [
            {
                "ip": "190.210.239.58",
                "action": "disconnect",
                "secuence": "2Yq18duRTRZlpzytAAAH",
                "timestamp": 1559771707829
            },
            {
                "ip": "190.210.239.58",
                "action": "connect",
                "secuence": "2Yq18duRTRZlpzytAAAH",
                "timestamp": 1559771429737
            },
            {
                "ip": "190.210.102.151",
                "action": "disconnect",
                "secuence": "7mkvFUsOcNPoTQmFAAAJ",
                "timestamp": 1559780011485
            },
            {
                "ip": "190.210.102.151",
                "action": "disconnect",
                "secuence": "UdvtJZCcdW2dXiF8AAAK",
                "timestamp": 1559820090165
            },
            {
                "ip": "190.210.102.151",
                "action": "disconnect",
                "secuence": "Q4pujOnC56B7O5LEAAAM",
                "timestamp": 1559820377869
            },
            {
                "ip": "80.28.110.143",
                "action": "disconnect",
                "secuence": "WDWU5Q-FV8zA1vwtAAAP",
                "timestamp": 1559826697683
            },
            {
                "ip": "216.199.92.50",
                "action": "disconnect",
                "secuence": "uec1naRjeqtik5WkAAAR",
                "timestamp": 1559829808424
            },
            {
                "ip": "190.210.102.151",
                "action": "connect",
                "secuence": "5OR0nthg2ZaO_vaeAAAS",
                "timestamp": 1559829718926
            },
            {
                "ip": "80.28.110.143",
                "action": "connect",
                "secuence": "b17lrBwRisVDG_jGAAAV",
                "timestamp": 1559832623223
            },
            {
                "ip": "216.199.92.50",
                "action": "connect",
                "secuence": "uec1naRjeqtik5WkAAAR",
                "timestamp": 1559828356432
            },
            {
                "action": "disconnect",
                "secuence": "DaaoPJkdopfHPDZjAAAQ",
                "timestamp": 1559826773030
            },
            {
                "ip": "80.28.110.143",
                "action": "connect",
                "secuence": "WAXMEOj3vE68TUo5AAAX",
                "timestamp": 1559832980961
            },
            {
                "ip": "80.28.110.143",
                "action": "disconnect",
                "secuence": "WAXMEOj3vE68TUo5AAAX",
                "timestamp": 1559832983345
            },
            {
                "ip": "80.251.161.186",
                "action": "disconnect",
                "secuence": "wLLWXDK_2Fi8gocxAAAY",
                "timestamp": 1559841345733
            },
            {
                "ip": "190.210.102.151",
                "action": "disconnect",
                "secuence": "A8n2Us-YWiFm9DQ8AAAZ",
                "timestamp": 1559844012144
            },
            {
                "ip": "190.210.102.151",
                "action": "connect",
                "secuence": "zi_6lySysVlj6pGwAAAa",
                "timestamp": 1559844013192
            },
            {
                "ip": "190.210.102.151",
                "action": "disconnect",
                "secuence": "zi_6lySysVlj6pGwAAAa",
                "timestamp": 1559844015634
            },
            {
                "ip": "190.210.102.151",
                "action": "connect",
                "secuence": "Lo_kgk_xlmZyNZqPAAAb",
                "timestamp": 1559844463291
            },
            {
                "ip": "190.210.102.151",
                "action": "disconnect",
                "secuence": "Lo_kgk_xlmZyNZqPAAAb",
                "timestamp": 1559844464528
            },
            {
                "ip": "190.210.102.151",
                "action": "disconnect",
                "secuence": "vJ7WcuP5pnJjKP3OAAAI",
                "timestamp": 1559777844048
            },
            {
                "ip": "190.210.102.151",
                "action": "connect",
                "secuence": "vJ7WcuP5pnJjKP3OAAAI",
                "timestamp": 1559776291787
            },
            {
                "ip": "190.210.102.151",
                "action": "connect",
                "secuence": "7mkvFUsOcNPoTQmFAAAJ",
                "timestamp": 1559779957619
            },
            {
                "ip": "190.210.102.151",
                "action": "connect",
                "secuence": "UdvtJZCcdW2dXiF8AAAK",
                "timestamp": 1559820061277
            },
            {
                "ip": "190.210.102.151",
                "action": "disconnect",
                "secuence": "GXE1mEF322rh3O_lAAAL",
                "timestamp": 1559820343908
            },
            {
                "ip": "190.210.102.151",
                "action": "disconnect",
                "secuence": "5OR0nthg2ZaO_vaeAAAS",
                "timestamp": 1559829771908
            },
            {
                "ip": "80.28.110.143",
                "action": "disconnect",
                "secuence": "W3H7qx7vuqARmlp4AAAN",
                "timestamp": 1559826662533
            },
            {
                "ip": "190.210.102.151",
                "action": "connect",
                "secuence": "GXE1mEF322rh3O_lAAAL",
                "timestamp": 1559820090605
            },
            {
                "ip": "190.210.102.151",
                "action": "connect",
                "secuence": "Q4pujOnC56B7O5LEAAAM",
                "timestamp": 1559820249545
            },
            {
                "ip": "80.28.110.143",
                "action": "connect",
                "secuence": "W3H7qx7vuqARmlp4AAAN",
                "timestamp": 1559826564987
            },
            {
                "ip": "80.28.110.143",
                "action": "connect",
                "secuence": "WDWU5Q-FV8zA1vwtAAAP",
                "timestamp": 1559826696140
            },
            {
                "ip": "216.199.92.50",
                "action": "connect",
                "secuence": "d-0f9EAUHZe5MJkSAAAT",
                "timestamp": 1559829809964
            },
            {
                "action": "disconnect",
                "secuence": "5-mth55vACbWj68MAAAO",
                "timestamp": 1559826627153
            },
            {
                "ip": "216.199.92.50",
                "action": "disconnect",
                "secuence": "d-0f9EAUHZe5MJkSAAAT",
                "timestamp": 1559832345024
            },
            {
                "action": "disconnect",
                "secuence": "r8fjw40hsxz1umm0AAAU",
                "timestamp": 1559832567086
            },
            {
                "ip": "80.28.110.143",
                "action": "disconnect",
                "secuence": "b17lrBwRisVDG_jGAAAV",
                "timestamp": 1559832820384
            },
            {
                "ip": "80.28.110.143",
                "action": "connect",
                "secuence": "PGnJ-pE_5SIXwtscAAAW",
                "timestamp": 1559832966603
            },
            {
                "ip": "80.28.110.143",
                "action": "disconnect",
                "secuence": "PGnJ-pE_5SIXwtscAAAW",
                "timestamp": 1559832968250
            },
            {
                "ip": "80.251.161.186",
                "action": "connect",
                "secuence": "wLLWXDK_2Fi8gocxAAAY",
                "timestamp": 1559841087993
            },
            {
                "ip": "190.210.102.151",
                "action": "connect",
                "secuence": "A8n2Us-YWiFm9DQ8AAAZ",
                "timestamp": 1559843585430
            }
        ]
    };

    describe('list', function(){
       it('should return a list with 3 reports', function(done){
            let mongooseFindstub = sinon.stub(ReportSchema, "find", function(){
                return mockFind;   
            });
            let Report = require('../../lib/report', {'Report': mongooseFindstub});
            Report.list().then(function(reportList){
                reportList.length.should.equal(3);
                done();
            })
        });
     });
     describe('find', function(){
        it('should return the report "Connections by ip" when I search for "connections_by_ip by" type', function(done){
            let mongooseFindOneStub = sinon.stub(ReportSchema, "findOne", function(query){
                if(query.type === 'connections_by_ip'){
                    return mockFindOne;   
                }
            });
            let Report = require('../../lib/report', {'Report': mongooseFindOneStub});
            Report.find('connections_by_ip').then(function(report){
                report.type.should.equal('connections_by_ip');
                done();
            })
        });
     });
     
     describe('generate', function(){
        let mongooseFindById = sinon.stub(ReportSchema, "findById", mockFindById);
        let mockEs = sinon.stub(esClient, "search", mockEsSearch);
        let Report = require('../../lib/report', {'Report': mongooseFindById, 'esClient': mockEs});

        it('should process connections_by_ip report', function(done){
            Report.generate('5cf469cc9db1c9619c6c8935', 'today').then( reportProcesed => {
                reportProcesed.length.should.equal(procesedReports.connections_by_ip.length);
                done();
            });
        });
        it('should process connections_by_hour report', function(done){
            Report.generate('5cf469cc9db1c9619c6c8934', 'today').then( reportProcesed => {
                reportProcesed.length.should.equal(procesedReports.connections_by_hour.length);
                done();
            });
        });
        it('should process detailed report', function(done){
            Report.generate('5cf469cc9db1c9619c6c8933', 'today').then( reportProcesed => {
                reportProcesed.length.should.equal(procesedReports.detailed.length);
                done();
            });
        });
    });


    
});