'use strict';

require('dotenv').load();

const app    = require('./app');
const logger = require('./lib/logger');

const mongoSettings = {
    host:     process.env.MONGODB_HOST,
    port:     process.env.MONGODB_PORT,
    db:       process.env.MONGODB_DB
};
app.initialize(logger)
    .then((application) => {
        application.listen(process.env.API_SERVER_PORT);
        logger.info('Your server is listening on port ' + process.env.API_SERVER_PORT);
        app.connectMongoose(mongoSettings).then( function(ok){
            logger.info('Mongoose connected');
        }).catch(err => {
            logger.error(err)
        });
    }).catch(err => {
        logger.error(err)
    });
