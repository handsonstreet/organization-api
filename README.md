# organization-api

## Install
`npm install`

## Start
`npm start`

## Test
`npm run test`

## Linting and test
`npm run ci`

**This command generates:**

1.  Coverage results in:`results/istanbul`

2.  Detailed testing reports in:`/results/mochaawesome-reports`


## Websocket communication

When the API receives a websocket connection then generates a connections_by_hour making an elasticsearch query and sends their value on the first socket message.

Then, for every connection or disconnection sends a broadcast message informing the count of connections stablished at this moment.

## Api endpoints

The API offers two endpoints, all the request are cached by 10 seconds in redis

**Get report List**
`
GET http://jberrettamoreno.com/api/report/
`
Returns all the report objects thar has stored in MongoDB.

**Generate report**
Searches in MongoDB the report by the informed id and perform the report query into ElasticSearch 
`
GET http://jberrettamoreno.com/api/report/{id}/generate/today
`

Every report result from elasticsearch it prosessed in memory and returns an array with the values.
