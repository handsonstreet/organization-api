/* eslint-disable consistent-return */
'use strict';

const provider = require('../provider');
const logger = require('../logger');

function listProviders(req, res, next){
    provider.list().then(function(providerList) {
        return res.json(providerList.data);
    }).catch(function(err) {
        logger.error(err);
        err.httpStatusCode = 500;
        return next(err);
    });
}
function addProvider(req, res, next){
    
    let name = req.swagger.params.body.value.name;
    let type = req.swagger.params.body.value.type;
    let provider_id = req.swagger.params.body.value.provider_id;
    
    provider.add(name, type, provider_id).then(function(addedProvider) {
        return res.json(addedProvider.data);
    }).catch(function(err) {
        logger.error(err);
        err.httpStatusCode = 500;
        return next(err);
    });
}
function updateProvider(req, res, next){
    const id = req.swagger.params.id.value;
    let name = req.swagger.params.body.value.name;
    let type = req.swagger.params.body.value.type;
    let provider_id = req.swagger.params.body.value.provider_id;

    provider.update(id, name, type, provider_id).then(function(updatedProvider) {
        return res.json(updatedProvider.data);
    }).catch(function(err) {
        logger.error(err);
        err.httpStatusCode = 500;
        return next(err);
    });
}
function removeProvider(req, res, next){
    const id = req.swagger.params.id.value;
    provider.remove(id).then(function(removedProvider) {
        return res.json(removedProvider.data);
    }).catch(function(err) {
        logger.error(err);
        err.httpStatusCode = 500;
        return next(err);
    });
}


function getProvider(req, res, next){
    const id = req.swagger.params.id.value;
    provider.get(id).then(function(foundedProvider) {
        return res.json(foundedProvider.data);
    }).catch(function(err) {
        logger.error(err);
        err.httpStatusCode = 500;
        return next(err);
    });
}


module.exports = {
    listProviders,
    addProvider,
    updateProvider,
    removeProvider,
    getProvider
};
