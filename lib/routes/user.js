/* eslint-disable consistent-return */
'use strict';

const user = require('../user');
const logger = require('../logger');

function listUsers(req, res, next) {
    user.list().then(function(users) {
        JSON.stringify(users.data);
        return res.json(users.data);
    }).catch(function(err) {
        logger.error(err);
        err.httpStatusCode = 500;
        return next(err);
    });
}

function addUser(req, res, next) {

    let first_name = req.swagger.params.body.value.first_name;
    let last_name = req.swagger.params.body.value.last_name;
    let email = req.swagger.params.body.value.email;
    let birth_day = req.swagger.params.body.value.birth_day;
    let identity_type = req.swagger.params.body.value.identity_type;
    let identity_number = req.swagger.params.body.value.identity_number;
    let role = req.swagger.params.body.value.role;
    let descrption = req.swagger.params.body.value.descrption;

    
    user.add(first_name, last_name, email, birth_day, identity_type, identity_number, role, descrption).then(function(addedUser) {
        return res.json(addedUser.data);
    }).catch(function(err) {
        logger.error(err);
        err.httpStatusCode = 500;
        return next(err);
    });
}

function getUser(req, res, next) {
    const id = req.swagger.params.id.value;
    user.get(id).then(function(foundedUser) {
        return res.json(foundedUser.data);
    }).catch(function(err) {
        logger.error(err);
        err.httpStatusCode = 500;
        return next(err);
    });
}

function updateUser(req, res, next) {
    const id = req.swagger.params.id.value;
    let first_name = req.swagger.params.body.value.first_name;
    let last_name = req.swagger.params.body.value.last_name;
    let email = req.swagger.params.body.value.email;
    let birth_day = req.swagger.params.body.value.birth_day;
    let identity_type = req.swagger.params.body.value.identity_type;
    let identity_number = req.swagger.params.body.value.identity_number;
    let role = req.swagger.params.body.value.role;
    let descrption = req.swagger.params.body.value.descrption;

    user.update(id, first_name, last_name, email, birth_day, identity_type, identity_number, role, descrption).then(function(updatedUser) {
        return res.json(updatedUser.data);
    }).catch(function(err) {
        logger.error(err);
        err.httpStatusCode = 500;
        return next(err);
    });
}
function removeUser(req, res, next) {
    const id = req.swagger.params.id.value;
    user.remove(id).then(function(removedUser) {
        return res.json(removedUser.data);
    }).catch(function(err) {
        logger.error(err);
        err.httpStatusCode = 500;
        return next(err);
    });
}


module.exports = {
    listUsers,
    addUser,
    updateUser,
    removeUser,
    getUser
};
