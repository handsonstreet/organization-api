/* eslint-disable consistent-return */
'use strict';

const Organization = require('../organization');
const logger = require('../logger');
const User = require('../user');

function list(req, res, next){
    Organization.list().then(function(data) {
        return res.json(data);
    }).catch(function(err) {
        logger.error(err);
        err.httpStatusCode = 500;
        return next(err);
    });
}
function add(req, res, next){
    
    let name = req.swagger.params.body.value.name;
    let address = req.swagger.params.body.value.address;
    Organization.add(name, address).then(function(data) {
        return res.json(data);
    }).catch(function(err) {
        logger.error(err);
        err.httpStatusCode = 500;
        return next(err);
    });
}
function update(req, res, next){
    const id = req.swagger.params.id.value;
    let name = req.swagger.params.body.value.name;
    let address = req.swagger.params.body.value.address;
    let providers = req.swagger.params.body.value.providers;
    let users = req.swagger.params.body.value.users;
    Organization.update(id, name, address, providers, users).then(function(data) {
        return res.json(data);
    }).catch(function(err) {
        logger.error(err);
        err.httpStatusCode = 500;
        return next(err);
    });
}
function remove(req, res, next){
    const id = req.swagger.params.id.value;
    Organization.remove(id).then(function(data) {
        return res.json(data);
    }).catch(function(err) {
        logger.error(err);
        err.httpStatusCode = 500;
        return next(err);
    });
}


function get(req, res, next){
    const id = req.swagger.params.id.value;
    Organization.get(id).then(function(data) {
        return res.json(data);
    }).catch(function(err) {
        logger.error(err);
        err.httpStatusCode = 500;
        return next(err);
    });
}

function addOrganizationUser(req, res, next){
    let organizationId = req.swagger.params.id.value;
    let userId = req.swagger.params.userId.value;
    Organization.addUser(organizationId, userId).then(function(data) {
        return res.json(data);
    });
}
function removeOrganizationUser(req, res, next){
    let organizationId = req.swagger.params.id.value;
    let userId = req.swagger.params.userId.value;
    Organization.removeUser(organizationId, userId).then(function(data) {
        return res.json(data);
    });
}

function addOrganizationProvider(req, res, next){
    let organizationId = req.swagger.params.id.value;
    let providerId = req.swagger.params.providerId.value;
    Organization.addProvider(organizationId, providerId).then(function(data) {
        return res.json(data);
    });
}
function removeOrganizationProvider(req, res, next){
    let organizationId = req.swagger.params.id.value;
    let providerId = req.swagger.params.providerId.value;
    Organization.removeProvider(organizationId, providerId).then(function(data) {
        return res.json(data);
    });
}

module.exports = {
    list,
    add,
    update,
    remove,
    get,
    addOrganizationUser,
    removeOrganizationUser,
    addOrganizationProvider,
    removeOrganizationProvider
};
