'use strict';

const Organization = require('../models/organization');
const logger = require('../logger');
const User = require('../user');
const Provider = require('../provider');

function list() {
    return Organization.find({}).exec();
}

function get(id) {
    let OrganizationValue = new Promise((resolve, reject) => {
        Organization.findById(id, function(err, organization) {
            if (err) {
                return reject(err);
            }
            if(!organization) {
                return reject({'message': 'The selected organization does not exist'});
            }
            return resolve(organization);
        });
    });
    return OrganizationValue;
}

function add(name, address) {
    var organization = new Organization({name, address, providers: [], users: []});
    return organization.save();
}

// eslint-disable-next-line max-params
function update(id, name, address, providers, users) {
    return Organization.findByIdAndUpdate(id, {name, address});
}

function remove(id) {
    return Organization.findByIdAndRemove(id);
}

function addUser(organizationId, userId){
    if(userId){
        return User.get(userId).then(foundedUser => {
            return Organization.findByIdAndUpdate(organizationId,
                {$push: {users: foundedUser.data}},
                {safe: true, upsert: true, new: true}
            );
        });
    }
}

function removeUser(organizationId, userId){
    if(userId){
        return User.get(userId).then(foundedUser => {
            logger.info(foundedUser.data._id);
            return Organization.findByIdAndUpdate(organizationId,
                { $pull: {users: { $in: [foundedUser.data._id] }   } },
                { multi: true, safe: true, upsert: true, new: true }
            );
        });
    }
}

function addProvider(organizationId, providerId){
    if(providerId){
        return Provider.get(providerId).then(foundedProvider => {
            return Organization.findByIdAndUpdate(organizationId,
                {$push: {providers: foundedProvider.data}},
                {safe: true, upsert: true, new: true}
            );
        });
    }
}

function removeProvider(organizationId, providerId){
    if(providerId){
        return Provider.get(providerId).then(foundedProvider => {
            logger.info(foundedProvider.data._id);
            return Organization.findByIdAndUpdate(organizationId,
                { $pull: {providers: { $in: [foundedProvider.data._id] }   } },
                { multi: true, safe: true, upsert: true, new: true }
            );
        });
    }
}

module.exports = {
    list,
    add,
    get,
    update,
    remove,
    addUser,
    removeUser,
    addProvider,
    removeProvider
};
