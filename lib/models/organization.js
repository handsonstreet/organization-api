'use strict';

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
let ObjectId = mongoose.Schema.Types.ObjectId;


const OrganizationSchema = new mongoose.Schema({
    name: String,
    address: String,
    providers: [{type:ObjectId, ref:'provider'}],
    users: [{type:ObjectId, ref:'user'}]
});
module.exports = mongoose.model('organization', OrganizationSchema);
