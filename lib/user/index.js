'use strict';

const logger = require('../logger');
const axios = require('axios');

const userApi = process.env.USER_API;

function list() {
    return axios(userApi);
}
function get(id) {
    return axios(userApi + '/' + id);
}

function add(first_name, last_name, email, birth_day, identity_type, identity_number, role, descrption) {
    return axios({
        method: 'post',
        url: userApi,
        data: {first_name, last_name, email, birth_day, identity_type, identity_number, role, descrption}
      });
}

// eslint-disable-next-line max-params
function update(id, first_name, last_name, email, birth_day, identity_type, identity_number, role, descrption) {
    return axios({
        method: 'put',
        url: userApi + '/' + id,
        data: {first_name, last_name, email, birth_day, identity_type, identity_number, role, descrption}
      });
}

function remove(id) {
    return axios({
        method: 'delete',
        url: userApi + '/' + id
    });
}
module.exports = {
    list,
    add,
    get,
    update,
    remove
};
