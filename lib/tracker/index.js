'use strict';
const logger = require('../logger');
var esClient = require('../elasticsearch');
    

function track(index, payload) {
    let trackerValue = new Promise((resolve, reject) => {
        logger.info('tracking ip ' + JSON.stringify(payload));
        esClient.index({
            index,
            type: '_doc',
            body: payload
        }).then((result) => {
            resolve(result);
        }).catch((error) => {
            reject(error);
        });
    });
    return trackerValue;
}

module.exports = {
    track
};
