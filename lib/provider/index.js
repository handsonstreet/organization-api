'use strict';

const logger = require('../logger');
const axios = require('axios');

const providerApi = process.env.PROVIDER_API;

function list() {
    return axios(providerApi);
}
function get(id) {
    return axios(providerApi + '/' + id);
}

function add(name, type, provider_id) {
    return axios({
        method: 'post',
        url: providerApi,
        data: {name, type, provider_id}
      });
}

// eslint-disable-next-line max-params
function update(id, name, type, provider_id) {
    return axios({
        method: 'put',
        url: providerApi + '/' + id,
        data: {name, type, provider_id}
      });
}

function remove(id) {
    return axios({
        method: 'delete',
        url: providerApi + '/' + id
    });
}
module.exports = {
    list,
    add,
    get,
    update,
    remove
};
