'use strict';

const logger = require('../logger');
const redis = require('redis');
const redisHost = process.env.API_REDIS_HOST;
const redisPort = process.env.API_REDIS_PORT;

let client = redis.createClient(redisPort, redisHost);

client.on('error', function(err) {
    logger.error(err);
});
client.on('error', function(err) {
    logger.error('Something went wrong ' + err);
    throw err;
});

function store(key, obj, duration) {
    let setValue = new Promise((resolve, reject) => {
        if(duration) {
            client.set(key, JSON.stringify(obj), 'EX', duration, (err, result) => {
                if (err) {
                    logger.error(err);
                    reject(err);
                }
                logger.info(result);
                resolve(result);
            });

        } else {
            client.set(key, JSON.stringify(obj), (err, result) => {
                if (err) {
                    logger.error('REDIS set error: ' + err);
                    reject(err);
                }
                logger.info('REDIS set result: ' + result);
                resolve(result);
            });
        }
    });
    return setValue;
}
function find(key) {
    let getValue = new Promise((resolve, reject) => {
        client.get(key, function(err, result) {
            if (err) {
                logger.error(err);
                reject(err);
            }
            resolve(JSON.parse(result));
        });
    });
    return getValue;
}

module.exports = {
    store,
    find
};
