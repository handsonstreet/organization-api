'use strict';

require('dotenv').load();
const express        = require('express');
const cors           = require('cors');
const compression    = require('compression');
const bodyParser     = require('body-parser');
const helmet         = require('helmet');
var winston = require('winston'),
    expressWinston = require('express-winston');

const swaggerTools   = require('swagger-tools');
const mongoose       = require('mongoose');
mongoose.Promise = global.Promise;   

function initialize(logger) {
    const app = express();

    if (process.env.API_IS_BEHIND_PROXY) {
        app.enable('trust proxy');
    }

    app.use(helmet.hidePoweredBy());
    app.use(helmet.ieNoOpen());
    app.use(helmet.noSniff());
    app.use(helmet.frameguard());
    app.use(helmet.xssFilter());
    app.use(compression());
    app.use(cors());
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());
    app.use(expressWinston.logger({
        transports: [
          new winston.transports.Console()
        ],
        format: winston.format.combine(
          winston.format.colorize(),
          winston.format.json()
        ),
        meta: true, // optional: control whether you want to log the meta data about the request (default to true)
        msg: "HTTP {{req.method}} {{req.url}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
        expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
        colorize: false, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
        ignoreRoute: function (req, res) { return false; } // optional: allows to skip some log messages based on request and/or response
      }));

    const swaggerDoc = require('./swagger/swagger.json');

    return new Promise(function(resolve) {
        swaggerTools.initializeMiddleware(swaggerDoc, function(middleware) {
            app.use(middleware.swaggerMetadata());

            app.use(middleware.swaggerValidator());

            app.use(middleware.swaggerUi());

            //Route validated requests to appropriate controller
            app.use(middleware.swaggerRouter({
                controllers:           './lib/routes',
                ignoreMissingHandlers: true,
                useStubs:              false // Conditionally turn on stubs (mock mode)
            }));


            app.use(function(err, req, res, next) {
                if (res.headersSent) {
                    return next(err);
                }

                res.status(err.status || 500);

                const error = {
                    errorCode:   res.statusCode,
                    userMessage: err.message
                };

                if (process.env.NODE_ENV === 'development') {
                    error.stack = err;
                }

                return res.json(error);
            });

            app.use(function(req, res) {
                res.status(404).json({
                    errorCode:   404,
                    userMessage: 'Not found.'
                });
            });

            resolve(app);
        });
    });

    
}
function connectMongoose(settings) {
    const mongoUrl = 'mongodb://' + settings.host + ':' + settings.port + '/' + settings.db;
    return mongoose.connect(mongoUrl,  { useMongoClient: true });
}

module.exports = {
    initialize,
    connectMongoose
};
